#!/bin/sh
#
# kiridashi.sh
#  A wrapper script for get_inv.pl and get_grib.pl
#  Both of perl programs are got from as following URLs;
#   ftp://ftp.cpc.ncep.noaa.gov/wd51we/fast_downloading_grib/get_inv.pl
#   ftp://ftp.cpc.ncep.noaa.gov/wd51we/fast_downloading_grib/get_grib.pl
#
# original script coded by Takashi Unuma
#              inspired by Hirohiko Ishikawa, Kyoto Univ.
#
# last modified: September 17, 2020
#
# --- encoding: utf-8-unix ---


if test $# -lt 3 ; then
    echo "USAGE: sh $(basename $0) YYYYMMDD iHH fHH [OUTPUT-name]"
    echo "   YYYYMMDD: please specify date as eight digits"
    echo "        iHH: please specify initial time as two digits"
    echo "        fHH: please specify forecast time as two digits"
    exit
fi

# 日時指定
YYYYMMDD=$1
# モデルの初期時刻を指定
iHH=$2
# モデルの予報時刻を指定
fHH=$3
# 出力ファイル名 (任意)
OUT=$4

# 解像度の指定 (内部パラメタ)
GFSRESO=0p50
SSTRESO=0.5

# アーカイブ URL の指定 (変更の必要なし)
URL=http://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod
#URL=http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod
#URL=ftp://ftpprd.ncep.noaa.gov/pub/data/nccf/com/gfs/prod

### 実行部分 ここから --->

# 引数の桁数をチェック
if test ${#YYYYMMDD} -ne 8 ; then
    echo "   YYYYMMDD: please specify date as 'EIGHT' digits"
    exit
fi
if test ${#iHH} -ne 2 ; then
    echo "   iHH: please specify initial time as 'TWO' digits"
    exit
fi
if test ${#fHH} -ne 3 ; then
    echo "   fHH: please specify forecast time as 'THREE' digits"
    exit
fi

# 解析値 (anl) と T = 00 h の判別
if test ${fHH} = "anl" ; then
    INPUT=gfs.t${iHH}z.pgrb2.${GFSRESO}.${fHH}
#    SST=rtgssthr_grb_${SSTRESO}.grib2
else
    INPUT=gfs.t${iHH}z.pgrb2.${GFSRESO}.f${fHH}
fi

# 出力名 (デフォルト値の指定)
if test ${#OUT} -eq 0 ; then
    if test ${fHH} = "anl" ; then
	OUT=gfs.${YYYYMMDD}${iHH}.pgrb2.${GFSRESO}.${fHH}
    else
	OUT=gfs.${YYYYMMDD}${iHH}.pgrb2.${GFSRESO}.f${fHH}
    fi
else
    OUT=${INPUT}
fi
#echo "OUTPUT FILE: ${OUT}"


# 処理部分
#  方針：
#   1. get_inv.pl を用いて，もとの GRIB2 ファイルの変数リストを読み込む
#   2. 1. で得た変数リストの中から，WRF の計算に必要な変数のみ awk を用いて切り出す
#   3. 2. で編集した変数リストをもとに，get_grib.pl を用いて切り出したファイルをダウンロード
# get_inv.pl ${URL}/gfs.${YYYYMMDD}/${iHH}/${INPUT}.idx | \
#     awk '/\:TMP\:/ || /\:RH\:/ || /\:HGT\:/ || /\:UGRD\:/ || /\:VGRD\:/ || /\:PRES\:/ || /\:PRMSL\:/ || /\:SOILW\:/ || /\:TSOIL\:/ || /\:WEASD\:/ || /\:SNOD\:/ || /\:ICEC\:/ || /\:LAND\:/ {print $0}' | \
#     awk '$1 !~ /planetary/ && $1 !~ /PV=/ && $1 !~ /max/ && $1 !~ /highest/ && $1 !~ /tropopause/ && $1 !~ /entire/ && $2 !~ /isotherm/ && $2 !~ /sigma/ {print $0}' | \
#     get_grib.pl ${URL}/gfs.${YYYYMMDD}/${iHH}/${INPUT} ${OUT}
wget -q -nc --wait=10 --tries=3 ${URL}/gfs.${YYYYMMDD}/${iHH}/${INPUT} -O ${OUT}

# SST
#  SST は 1 変数 (TMP のみ) なのでそのまま値を get_grib.pl へ渡す
#  実行は 解析時刻 (anl) のみ
#if test ${fHH} = "anl" ; then
#    get_inv.pl ${URL}/sst.${YYYYMMDD}/${SST}.idx | get_grib.pl ${URL}/sst.${YYYYMMDD}/${SST} ${SST}
#fi

# 実行部分 ここまで <---

exit 0
