#!/bin/bash
#
# change_time.sh
# original script coded by Takashi Unuma, Kyoto Univ.
# Last modified: 2013/12/01
#
 
if test $# -lt 3 ; then
    echo "USAGE: $(basename $0) [YYYYMMDDHHNN] [INT] [+/-]"
    echo " *** Note: All values must be 'integer'. *** "
    exit 1
fi
 
# Input TIME that must be '12 digits' with the format of 'YYYYMMDDHHNN'
INPUTDATE=$1
 
# time difference, unit is 'second'
INT=$2
 
# flag must be '+' or '-'
FLAG=$3
 
# convert from TIME to UNIXTIME
UTIME=$(date +%s --date "${INPUTDATE:0:4}-${INPUTDATE:4:2}-${INPUTDATE:6:2} ${INPUTDATE:8:2}:${INPUTDATE:10:2}")
 
# Output modified TIME
echo ${UTIME} ${INT} | awk '{print $1'${FLAG}'$2}'  | awk '{print strftime("%Y%m%d%H%M",$1)}'
