#!/bin/bash
#
# check_env.sh
#
# coded by Takashi Unuma
# last modified: 2015/02/06
#

exelist="geogrid.exe ungrib.exe metgrid.exe real.exe wrf.exe"
dirlist="geog"

count=0
for exe in ${exelist} ; do
    if test ! -s "${exe}" ; then
	echo "There is no ${exe}"
	echo "Please create a symbolic link of ${exe}"
	exit
    else
	count=$(expr ${count} + 1)
    fi
done

for dir in ${dirlist} ; do
    if test ! -s "${dir}" ; then
	echo "There is no ${dir}"
	echo "Please create a symbolic link of ${dir}"
	exit
    else
	count=$(expr ${count} + 1)
    fi
done

if test ${count} -eq 6 ; then
    echo "Configurations are fine!"
fi
