#!/bin/bash -x
#
# fcst_ghana.sh
#
# original script coded by Takashi Unuma
#              inspired by Hirohiko Ishikawa, Kyoto Univ.
#
# last modified: September 17, 2020
#

# Execution directory
readonly EXEDIR=${HOME}/fcst_ghana

# Number of MPI run (depending on CPU Cores in your machine)
readonly NUMMPI=2

# the forecast time in second (run 72 hours, which means 60*60*72 seconds)
readonly FORECAST_TIME=259200
#readonly FORECAST_TIME=21600 # forecast 6 hours (60*60*6 sec) for test run

# the forecast time in hour
readonly FORECAST_TIMEH=$(echo ${FORECAST_TIME} | awk '{print $1/3600}')

# input file interval in second (6 hours, which means 60*60*6 seconds [recommended])
readonly FORECAST_INT=21600
# input file interval in hour
readonly FORECAST_INTH=$(echo ${FORECAST_TIME} | awk '{print $1/3600}')

# Specify the date and time (the date and time automatically defined as following command)
readonly UTCTIME=$(TZ=UTC date --date '6 hours ago' +%Y%m%d%H)

# WWW directory to display the WRF forecasts
readonly WWWDIR=/var/www/html

# debug for the format of date and time
if test ${#UTCTIME} -ne 10 ; then
    echo " YYYYMMDDHH: please specify date and time as ten digits"
    exit
fi

# debug option
#  0: disable debug option
#  1: enable debug option
debug=0 

# check start time of this script
stime=$(date +%s)

# --- unucommand(s) (the command(s) that is coded by Takashi Unuma)
calctime=${EXEDIR}/change_time.sh

# --- exports environmental settings
source ${HOME}/.bashrc

export HOMEOPT="${HOME}/opt"

## -- NetCDF 4
export NETCDF=${HOMEOPT}
export NCHOME=${NETCDF}
export NETCDF_PATH=${NETCDF}
export NETCDFHOME=${NETCDF}
export NETCDFROOT=${NETCDF}
export PATH="${NETCDF}/bin:${PATH}"
export LD_LIBRARY_PATH="${NETCDF}/lib:${LD_LIBRARY_PATH}"
export MANPATH="${NETCDF}/man:${MANPATH}"

## -- HDF5
export HDF5=${HOMEOPT}
export HDF=${HOMEOPT}
export PATH="${HDF}/bin:${PATH}"
export LD_LIBRARY_PATH="${HDF}/lib:${LD_LIBRARY_PATH}"
export INCLUDE="${HDF}/include:${INCLUDE}"
export MANPATH="${HDF}/man:${MANPATH}"

## -- ZLIB
export ZLIB=${HOMEOPT}
export LD_LIBRARY_PATH="${ZLIB}/lib:${LD_LIBRARY_PATH}"
export INCLUDE="${ZLIB}/include:${INCLUDE}"

## -- NCL/NCARG
export NCARG="/usr"
export NCARG_ROOT="${NCARG}"
export NCARG_BIN="${NCARG}/bin"
export NCARG_LIB="${NCARG}/lib"
export NCARG_INCLUDE="${NCARG}/include"
export NCLCOMMAND="${NCARG}/bin/idt"
export NCL_COMMAND="${NCARG}/bin/ncl"
export PATH="${NCARG}/bin:${PATH}"
export LD_LIBRARY_PATH="${NCARG}/lib:${LD_LIBRARY_PATH}"
export NCL="${NCARG}"
export PATH="${NCL}/bin:$PATH"

# - for kiridashi.sh using get_inv.pl and get_grib.pl in bin/ dir.
export PATH="${PATH}:${EXEDIR}/bin"


###### EXECUTE SECTION FROM HERE ######
# debug option
if test ${debug} -eq 1 ; then
    set -eu
else
    set +e
fi

# go to the working directory
cd ${EXEDIR}

# define both start time and end time for the forcast
# --- start date and time
STIME=${UTCTIME}
SYYYY=${STIME:0:4}
SMM=${STIME:4:2}
SDD=${STIME:6:2}
SHH=${STIME:8:2}
# --- end date and time
ETIME=$(${calctime} ${STIME}00 ${FORECAST_TIME} +)
EYYYY=${ETIME:0:4}
EMM=${ETIME:4:2}
EDD=${ETIME:6:2}
EHH=${ETIME:8:2}


# edit namelist.wps
#  The tamplate file is "namelist.wps_org". 
/bin/sed -e "s/SYYYY-SMM-SDD_SHH/${SYYYY}-${SMM}-${SDD}_${SHH}/g" \
         -e "s/EYYYY-EMM-EDD_EHH/${EYYYY}-${EMM}-${EDD}_${EHH}/g" ${EXEDIR}/namelist.wps_org > ${EXEDIR}/namelist.wps

# edit namelist.input
#  The tamplate file is "namelist.input_org". 
/bin/sed -e "s/SYYYY/${SYYYY}/g" \
         -e "s/SMM/${SMM}/g" \
         -e "s/SDD/${SDD}/g" \
         -e "s/SHH/${SHH}/g" \
         -e "s/EYYYY/${EYYYY}/g" \
         -e "s/EMM/${EMM}/g" \
         -e "s/EDD/${EDD}/g" \
         -e "s/EHH/${EHH}/g" ${EXEDIR}/namelist.input_org > ${EXEDIR}/namelist.input


# display summary of this forecast
/bin/echo "SUMMARY OF THIS FORECAST"
/bin/echo "  WORK DIR : ${EXEDIR}"
/bin/echo " START TIME: ${STIME:0:10}"
/bin/echo "   END TIME: ${ETIME:0:10}"
/bin/echo "  FCST HOUR: ${FORECAST_TIMEH} H"
/bin/echo ""


# get GFS data
st_gfs_get=$(date +%s)
/bin/echo "DOWNLOAD GFS DATA"
# --- analysis time
/bin/echo " Now getting GFS data (ANL)..."
# - if there is no GFS data on the current directory, execute kiridashi.sh
#   kiridashi.sh is going to minimize the download cost even if the slower internet speed. 
/bin/bash ${EXEDIR}/kiridashi.sh ${SYYYY}${SMM}${SDD} ${SHH} anl ""
maxtrynum=3
for (( trynum=1 ; trynum<=${maxtrynum} ; trynum++ )) ; do
    if test ! -s "${EXEDIR}/gfs.${SYYYY}${SMM}${SDD}${SHH}.pgrb2.0p50.anl" ; then
        /bin/bash ${EXEDIR}/kiridashi.sh ${SYYYY}${SMM}${SDD} ${SHH} anl ""
    else
        /bin/echo "  ${EXEDIR}/gfs.${SYYYY}${SMM}${SDD}${SHH}.pgrb2.0p50.anl is found"
        trynum=3
    fi
done
# --- forecast time
# - do processes recursively
for (( i=0 ; i<=${FORECAST_TIMEH} ; i=${i}+6 )) ; do
    # - change the format of the forecast time in hour
    fhh=$(echo ${i} | awk '{printf ("%03d\n",$1)}')
    /bin/echo " Now getting GFS data (FT=${fhh})..."
    # - if there is no GFS data on the current directory, execute kiridashi.sh
    /bin/bash ${EXEDIR}/kiridashi.sh ${SYYYY}${SMM}${SDD} ${SHH} ${fhh} ""
    # maxtrynum means that it will be tried to get the GFS data at least 3 times if it is not downloaded. 
    maxtrynum=3
    for (( trynum=1 ; trynum<=${maxtrynum} ; trynum++ )) ; do
        if test ! -s "${EXEDIR}/gfs.${SYYYY}${SMM}${SDD}${SHH}.pgrb2.0p50.f${fhh}" ; then
            /bin/bash ${EXEDIR}/kiridashi.sh ${SYYYY}${SMM}${SDD} ${SHH} ${fhh} ""
        else
            /bin/echo "  ${EXEDIR}/gfs.${SYYYY}${SMM}${SDD}${SHH}.pgrb2.0p50.f${fhh} is found"
            trynum=3
        fi
    done
done
et_gfs_get=$(date +%s)
/bin/echo ""


# WPS/WRF SECTIONs
/bin/echo "GO TO WPS/WRF SECTIONS"
st_wps=$(date +%s)
# execute geogrid.exe (create topography data)
/bin/echo " WPS: GEOGRID"
# - execute geogrid.exe
${EXEDIR}/geogrid.exe
# - check output
if [ ! -s ${EXEDIR}/geo_em.d01.nc ] ; then
    /bin/echo "Failed GEOGRID section"
    exit 1
fi

# execute ungrib.exe for GFS data
/bin/echo " WPS: UNGRIB (GFS)"
# - remove temporary files that is created by link_grib.csh
/bin/rm -f ${EXEDIR}/GRIBFILE.???
# - create symbolic link files for GFS files
${EXEDIR}/link_grib.csh ${EXEDIR}/gfs.${SYYYY}${SMM}${SDD}${SHH}.*
# - run ungrib.exe
${EXEDIR}/ungrib.exe
# --- check outputs
if [ ! -s ${EXEDIR}/GFS:${SYYYY}-${SMM}-${SDD}_${SHH} ] ; then
    /bin/echo "Failed UNGRIB (GFS) section"
    exit 1
else
    /bin/rm -f GRIBFILE.???
fi
    
# execute metgrid.exe
/bin/echo " WPS: METGRID"
# - run metgrid.exe (create horizontally interpolated data for creating initial/boundary conditions, but the data is not vertically interpolated)
${EXEDIR}/metgrid.exe
# - check output
if [ ! -s ${EXEDIR}/met_em.d01.${SYYYY}-${SMM}-${SDD}_${SHH}:00:00.nc ] ; then
    /bin/echo "Failed METGRID section"
    exit 1
fi
et_wps=$(date +%s)


st_wrf=$(date +%s)
# execute real.exe
/bin/echo " WRF: REAL"
# - run real.exe (create initial/boundary conditions with interpolating vertically for running main core of WRF model)
mpirun -np ${NUMMPI} ${EXEDIR}/real.exe >& /dev/null 2>&1
# - check output
if [ ! -s wrfbdy_d01 ] && [ ! -s wrfinput_d01 ] ; then
    /bin/echo "Failed WRF REAL section"
    exit 1
fi

# execute wrf.exe
/bin/echo " WRF: START FORECAST"
# - run wrf.exe (simulate the time series of 3-dimensional variables at spesified time step that is defined in the namelist.input)
mpirun -np ${NUMMPI} ${EXEDIR}/wrf.exe >& /dev/null 2>&1
et_wrf=$(date +%s)


# POST-PROCESS
#  - these processes are going to draw figures by using the forecast outputs. 
#  - it is capable to customize in this section as you want!
st_post=$(date +%s)

# --- process for Domain 01
for idomain in d01 d02 ; do
    flag=$(ls ${EXEDIR}/wrfout_${idomain}_${SYYYY}-${SMM}-${SDD}_${SHH}:00:00 | wc -l)
    if test ${flag} -eq 1 ; then
        rm -f ${EXEDIR}/wrfout.nc
        /bin/ln -s ${EXEDIR}/wrfout_${idomain}_${SYYYY}-${SMM}-${SDD}_${SHH}:00:00 ${EXEDIR}/wrfout.nc

        # - draw RH at 925 hPa
        ${NCARG}/bin/ncl wrf_rh_PressureLevel_925.ncl >& /dev/null 2>&1
        # - draw RH at 850 hPa
        ${NCARG}/bin/ncl wrf_rh_PressureLevel_850.ncl >& /dev/null 2>&1
        # - draw RH at 700 hPa
        ${NCARG}/bin/ncl wrf_rh_PressureLevel_700.ncl >& /dev/null 2>&1
        # - draw RH at 500 hPa
        ${NCARG}/bin/ncl wrf_rh_PressureLevel_500.ncl >& /dev/null 2>&1
        # - draw RH at 200 hPa
        ${NCARG}/bin/ncl wrf_rh_PressureLevel_200.ncl >& /dev/null 2>&1

        # - draw Winds at 925 hPa
        ${NCARG}/bin/ncl wrf_ws_PressureLevel_925.ncl >& /dev/null 2>&1
        # - draw Winds at 850 hPa
        ${NCARG}/bin/ncl wrf_ws_PressureLevel_850.ncl >& /dev/null 2>&1
        # - draw Winds at 700 hPa
        ${NCARG}/bin/ncl wrf_ws_PressureLevel_700.ncl >& /dev/null 2>&1
        # - draw Winds at 500 hPa
        ${NCARG}/bin/ncl wrf_ws_PressureLevel_500.ncl >& /dev/null 2>&1
        # - draw Winds at 200 hPa
        ${NCARG}/bin/ncl wrf_ws_PressureLevel_200.ncl >& /dev/null 2>&1

        # - draw pre-1-hour rainfall [mm]
        ${NCARG}/bin/ncl wrf_Precip3.ncl >& /dev/null 2>&1
        # - draw muCAPE [J/kg]
        ${NCARG}/bin/ncl wrf_cape.ncl >& /dev/null 2>&1
        # - draw SReH [m^2/s^2]
        ${NCARG}/bin/ncl wrf_helicity.ncl >& /dev/null 2>&1
        # - draw PV [PUV]
        #${NCARG}/bin/ncl wrf_pv300.ncl >& /dev/null 2>&1
        # - draw SkewT for Accra
        ${NCARG}/bin/ncl wrf_SkewT_accra.ncl >& /dev/null 2>&1
        # - draw SkewT for Tamale
        ${NCARG}/bin/ncl wrf_SkewT_tamale.ncl >& /dev/null 2>&1
        # - draw pressure at sfc
        ${NCARG}/bin/ncl wrf_sfcprs.ncl >& /dev/null 2>&1

        # - split conbined PS file in twenty five files
        /usr/bin/psplit plt_rh_PressureLevel_925.ps rh925_
        /usr/bin/psplit plt_rh_PressureLevel_850.ps rh850_
        /usr/bin/psplit plt_rh_PressureLevel_700.ps rh700_
        /usr/bin/psplit plt_rh_PressureLevel_500.ps rh500_
        /usr/bin/psplit plt_rh_PressureLevel_200.ps rh200_
        /usr/bin/psplit plt_ws_PressureLevel_925.ps ws925_
        /usr/bin/psplit plt_ws_PressureLevel_850.ps ws850_
        /usr/bin/psplit plt_ws_PressureLevel_700.ps ws700_
        /usr/bin/psplit plt_ws_PressureLevel_500.ps ws500_
        /usr/bin/psplit plt_ws_PressureLevel_200.ps ws200_
        /usr/bin/psplit plt_precip.ps rain_
        /usr/bin/psplit plt_cape.ps cape_
        /usr/bin/psplit plt_helicity.ps sreh_
        #/usr/bin/psplit plt_pv.ps pv300_
        /usr/bin/psplit plt_SkewT_accra.ps skewt_accra_
        /usr/bin/psplit plt_SkewT_tamale.ps skewt_tamale_
        /usr/bin/psplit plt_sfcprs.ps sfcprs_

        # - convert the format of figures from PS to PNG with the options of croping it and setting resolution to 144 dpi
        /bin/ls ./{rh925,rh850,rh700,rh500,rh200,ws925,ws850,ws700,ws500,ws200,rain,cape,sreh,skewt_accra,skewt_tamale,sfcprs}_????.ps | /usr/bin/parallel --gnu -j +0 convert {} {.}.png
        /bin/ls ./{rh925,rh850,rh700,rh500,rh200,ws925,ws850,ws700,ws500,ws200,rain,cape,sreh,skewt_accra,skewt_tamale,sfcprs}_????.ps | /usr/bin/parallel --gnu -j +0 gmt psconvert -Tf {}

        # - create PDF
        /usr/bin/ps2pdf plt_sfcprs.ps
        /usr/bin/ps2pdf plt_precip.ps
        /usr/bin/ps2pdf plt_rh_PressureLevel_925.ps
        /usr/bin/ps2pdf plt_rh_PressureLevel_850.ps
        /usr/bin/ps2pdf plt_rh_PressureLevel_700.ps
        /usr/bin/ps2pdf plt_rh_PressureLevel_500.ps
        /usr/bin/ps2pdf plt_rh_PressureLevel_200.ps
        /usr/bin/ps2pdf plt_ws_PressureLevel_925.ps
        /usr/bin/ps2pdf plt_ws_PressureLevel_850.ps
        /usr/bin/ps2pdf plt_ws_PressureLevel_700.ps
        /usr/bin/ps2pdf plt_ws_PressureLevel_500.ps
        /usr/bin/ps2pdf plt_ws_PressureLevel_200.ps

        # - copy figures to www dir in order to see from web
        /usr/bin/rsync -ahv ./{rh925,rh850,rh700,rh500,rh200,ws925,ws850,ws700,ws500,ws200,rain,cape,sreh,skewt_accra,skewt_tamale,sfcprs}_????.{png,pdf,ps} ${WWWDIR}/${idomain}/
        /usr/bin/rsync -ahv ./plt_{sfcprs,precip}.pdf ${WWWDIR}/${idomain}/
        /usr/bin/rsync -ahv ./plt_{rh,ws}_PressureLevel_{925,850,700,500,200}.pdf ${WWWDIR}/${idomain}/
        mkdir -p ./${UTCTIME}/${idomain}/
        /usr/bin/rsync -ahv ./{rh925,rh850,rh700,rh500,rh200,ws925,ws850,ws700,ws500,ws200,rain,cape,sreh,skewt_accra,skewt_tamale,sfcprs}_????.{png,pdf,ps} ./${UTCTIME}/${idomain}/
        /usr/bin/rsync -ahv ./plt_{sfcprs,precip}.pdf ./${UTCTIME}/${idomain}/
        /usr/bin/rsync -ahv ./plt_{rh,ws}_PressureLevel_{925,850,700,500,200}.pdf ./${UTCTIME}/${idomain}/
        /bin/rm ./*.{ps,png,pdf}
    else
        echo "ERROR: ${EXEDIR}/wrfout_${idomain}_${SYYYY}-${SMM}-${SDD}_${SHH}:00:00 is not found."
        #exit 1
    fi
done

# - copy outputs (wrfout_d0?_*) to the directory of the name of YYYYMMDDHH which is going to be deleted after a week
/usr/bin/rsync -ahv ./rsl.* ./wrfout_d0?_* ./process.log ./${UTCTIME}/

# CLEAN
${EXEDIR}/clean.sh

et_post=$(date +%s)
/bin/echo ""

etime=$(date +%s)a

/bin/echo "    TOTAL: " $(echo ${stime}      ${etime}      | awk '{print $2-$1}') " [sec]"
/bin/echo " DOWNLOAD: " $(echo ${st_gfs_get} ${et_gfs_get} | awk '{print $2-$1}') " [sec]"
/bin/echo "      WPS: " $(echo ${st_wps}     ${et_wps}     | awk '{print $2-$1}') " [sec]"
/bin/echo "      WRF: " $(echo ${st_wrf}     ${et_wrf}     | awk '{print $2-$1}') " [sec]"
/bin/echo " POSTPROC: " $(echo ${st_post}    ${et_post}    | awk '{print $2-$1}') " [sec]"

# - copy process.log to www dir.
/bin/cp process.log process_${UTCTIME}.log
/usr/bin/rsync -ahv ./process_${UTCTIME}.log ${WWWDIR}/
