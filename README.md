# ガーナ域予報計算プログラムのセットアップについて
---

__written by Takashi Unuma__

__inspired by Hirohiko Ishikawa, Kyoto Univ.__

__last modified: 2015/03/16__

---

## 概要
- 設定方法
- プログラムの概要
- TODO
- その他

---

## 設定方法

### 手順
まず fcst_ghana.tar.gz を展開してください．
```
 $ tar -zxvf fcst_ghana.tar.gz
```

メインスクリプトは，以下の 2 つになります．
```
 fcst_ghana.sh: テスト用 shell script (動作確認用: 日時を指定して実行する形式)
 fcst_ghana_6h.sh: cron 設定用 shell script (cron 用: date コマンドを用いて日時を自動的に取得する形式)
```
上記のファイル内に作業ディレクトリを EXEDIR という名前で指定して下さい．
この作業ディレクトリが，実際に予報計算を行う時に使用するディレクトリとなります．
そして，上記で指定した作業ディレクトリ内に fcst_ghana.tar.gz 内のファイルを全て置いて下さい．

WRF/WPS の実行ファイルのシンボリックリンクを EXEDIR へ貼ってくっださい．
```
 $ ln -s [path-to-WPS-dir]/geogrid.exe [EXEDIR]/
 $ ln -s [path-to-WPS-dir]/ungrib.exe [EXEDIR]/
 $ ln -s [path-to-WPS-dir]/metgrid.exe [EXEDIR]/
 $ ln -s [path-to-WRF-dir]/real.exe [EXEDIR]/
 $ ln -s [path-to-WRF-dir]/wrf.exe [EXEDIR]/ 
```

テスト
cron での実行を行う前に，テストを行います．
テストは，テスト用のメインスクリプト (fcst_ghana.sh) を用います．
どこで停止したか分かるように，bash をデバッグモードで以下のように実行します．
```
 $ bash -x fcst_ghana.sh 2015031600 # 実行する当日の日時を指定する
```

cron の設定
cron の設定には，run\_6h.sh というラッパースクリプトを使用します．
まず，cron の設定モードに入ります．
```
 $ crontab -e 
```

設定モードに入ったら，以下のように設定します．
```
10 3,9,15,21 * * * /bin/bash /[path-to-EXEDIR]/run_6h.sh 
```
> 上記は，OS の Time zone における 3,9,15,21 時 10 分に run_6h.sh を実行します．
>
> run_6h.sh は fcst_ghana_6h.sh を実行するだけの shell script です．

---

## スクリプトの概要
スクリプト (fcst\_ghana.sh, fcst_ghana\_6h.sh) は以下を順に実行します．
- GFS データのダウンロード (kiridashi.sh を使用)
- WPS の実行 (geogrid.exe, ungrib.exe, and metgrid.exe の実行)
- WRF の実行 (real.exe, wrf.exe の実行)
- 後処理 (wrf_PressureLevel1.ncl, wrf_Precip3.ncl, wrf_cape.ncl, wrf_helicity.ncl, wrf_pv.ncl の実行)

> *.ncl は NCL スクリプトです
> 
> 画像は Postscript で出力された後，PNG 形式に変換されます

---

## TODO
- [SST Update](http://www2.mmm.ucar.edu/wrf/OnLineTutorial/CASES/SST/wrf.htm) の導入
- parallel コマンドの設定 (処理速度向上の為)

---

## その他

### 予報計算時のスケジュール案
```
               時間軸
               +---------+---------+---------+---------+--->
REAL TIME      00        06        12        18        00
GFS UPDATE               00        06        12        18
FCST START               06:10     12:10     18:10     00:10

GFS のデータは約 6 時間遅れで全てのデータが更新される．
そのため，上記の最終更新日時のデータを取得して 6 時間遅れた時刻から予報を行う．
```

### 計算時間等
72 時間予報の場合 (KUINS ネットワーク内)

- 全体の所要時間は 1 時間 20 分程度
- GFS データの取得に 30 分程度時間を要する (14 [個] のデータ)
- WRF/WPS の計算時間は 72 時間予報で 40 分程度
- その他の処理時間は 10 分程度

### 変更履歴
- kiridashi.sh 内で更に不必要な変数を削除 (ダウンロード時間の短縮)
- 後処理用に NCL スクリプトを追加 (CAPE, PV, SREH, Precip)
- Web 表示用の Javascript [Rhinoslider](http://www.rhinoslider.com/) を追加
- 計算設定・予報スケジュールのページを追加

### メモ
- 72 時間予報データの更新は初期時刻から約 4 時間後 (e.g. 00 UTC のデータは 04 UTC にはアーカイブされている)
- SST データは 00 UTC のみ
- GFS 予報値には SKINTEMP が含まれる
- GFS 予報値には土壌水分量・土壌気温の情報が含まれる
- GFS データの ungrib には最新版の [Vtable ファイル](http://www2.mmm.ucar.edu/wrf/src/Vtable.GFS_new) を使用する ([情報源](http://forum.wrfforum.com/viewtopic.php?f=12&t=8494))
- Postscript の変換処理を ghost script で行うように変更
