#!/bin/sh

rm -fv gfs.* 
rm -fv geo_em.d??.nc GRIBFILE.??? GFS:* PFILE:* met_em.d* 
rm -fv wrfbdy_d?? wrfinput_d?? wrflowinp_d?? wrfout_* wrfout.nc 
rm -fv rsl.{out,error}.???? 
rm -fv {geogrid,ungrib,metgrid}.log 
rm -fv *_???.{ps,png} plt_*.ps
rm -fv namelist.{wps,input,output}
